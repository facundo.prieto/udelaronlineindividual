﻿using dal;
using System.Collections.Generic;
using System.Linq;

namespace bl
{
    public class CursoManager
    {
        public List<Curso> lists()
        {
            using (var ctx = new MyContext())
            {
                var cursos = ctx.Cursos.ToList();
                return cursos;
            }
        }

        public void add(Curso curso)
        {
            using (var ctx = new MyContext())
            {
                ctx.Cursos.Add(curso);
                ctx.SaveChanges();
            }
        }

        public void delete(int id)
        {
            using (var ctx = new MyContext())
            {
                var curso = ctx.Cursos.SingleOrDefault(c => c.Id == id);

                ctx.Cursos.Remove(curso);
                ctx.SaveChanges();
            }
        }

        public Curso get(int id)
        {
            using (var ctx = new MyContext())
            {
                var curso = ctx.Cursos.SingleOrDefault(c => c.Id == id);
                return curso;
            }
        }

        public void edit(Curso curso)
        {
            using (var ctx = new MyContext())
            {
                var curso_update = ctx.Cursos.SingleOrDefault(c => c.Id == curso.Id);
                curso_update.Nombre = curso.Nombre;
                curso_update.Descripcion = curso.Descripcion;
                ctx.SaveChanges();
            }
        }

        public List<Curso> getCursosEnFacultad(int idFacu)
        {
            using (var ctx = new MyContext())
            {
                var cursos = ctx.Cursos.Where(c => c.Facultad.Id == idFacu).ToList<Curso>();
                return cursos;
            }
        }
    }
}