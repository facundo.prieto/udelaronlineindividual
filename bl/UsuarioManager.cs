﻿using dal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace bl
{
    public class UsuarioManager
    {
        public List<Usuario> lists()
        {
            using (var ctx = new MyContext())
            {
                var usuarios = ctx.Usuarios.ToList();
                return usuarios;
            }
        }

        public void add(Usuario usu)
        {
            using (var ctx = new MyContext())
            {
                ctx.Usuarios.Add(usu);
                ctx.SaveChanges();
            }
        }

        public void delete(int id)
        {
            using (var ctx = new MyContext())
            {
                var usu = ctx.Usuarios.SingleOrDefault(u => u.Id == id); // SELECT * FROM Usuario WHERE Id = @ID

                ctx.Usuarios.Remove(usu); //DELETE .. WHERE Id =
                ctx.SaveChanges();
            }
        }

        public Usuario get(int id)
        {
            using (var ctx = new MyContext())
            {
                var usu = ctx.Usuarios.SingleOrDefault(u => u.Id == id);
                return usu;
            }
        }

        public void edit(Usuario usu)
        {
            using (var ctx = new MyContext())
            {
                var usu_update = ctx.Usuarios.SingleOrDefault(u => u.Id == usu.Id);
                usu_update.Nombre = usu.Nombre;
                usu_update.Apellido = usu.Apellido;
                usu_update.Correo = usu.Correo;
                usu_update.Telefono = usu.Telefono;
                usu_update.Rol = usu.Rol;
                ctx.SaveChanges();
            }
        }
        
        public List<Usuario> getUsuariosEnFacultad(int idFacu)
        {
            using (var ctx = new MyContext())
            {
                var usus = ctx.Usuarios.Where(u => u.Facultad.Id == idFacu).ToList<Usuario>();
                return usus;
            }
        }
    }
}