﻿using dal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace bl
{
    public class FacultadManager
    {
        public List<Facultad> lists()
        {
            using (var ctx = new MyContext())
            {
                var facultades = ctx.Facultades.ToList();
                return facultades;
            }
        }

        public void add(Facultad facultad)
        {
            using (var ctx = new MyContext())
            {
                ctx.Facultades.Add(facultad);
                ctx.SaveChanges();
            }
        }

        public void delete(int id)
        {
            using (var ctx = new MyContext())
            {
                var facultad = ctx.Facultades.SingleOrDefault(f => f.Id == id);

                ctx.Facultades.Remove(facultad);
                ctx.SaveChanges();
            }
        }

        public Facultad get(int id)
        {
            using (var ctx = new MyContext())
            {
                var facultad = ctx.Facultades.SingleOrDefault(f => f.Id == id);
                return facultad;
            }
        }

        public void edit(Facultad facultad)
        {
            using (var ctx = new MyContext())
            {
                var facultad_update = ctx.Facultades.SingleOrDefault(f => f.Id == facultad.Id);
                facultad_update.Nombre = facultad.Nombre;
                facultad_update.Abreviatura = facultad.Abreviatura;
                ctx.SaveChanges();
            }
        }

        public void agregarCurso(int IdFacu, int idCurso)
        {
            using (var ctx = new MyContext())
            {
                var facultad_update = ctx.Facultades.SingleOrDefault(f => f.Id == IdFacu);
                var curso = ctx.Cursos.SingleOrDefault(c => c.Id == idCurso);

                //estos dos controles no deberian ejecutarse xq deberia controlar que exista en el frontend
                if (facultad_update.Cursos == null || !facultad_update.Cursos.Any())
                {
                    facultad_update.Cursos = new List<Curso>();
                }
                if (curso == null)
                {
                    curso = new Curso();
                }

                facultad_update.Cursos.Add(curso);
                ctx.SaveChanges();
            }
        }

        public void agregarEstudiante(int IdFacu, int idUsuario)
        {
            using (var ctx = new MyContext())
            {
                var facultad_update = ctx.Facultades.SingleOrDefault(f => f.Id == IdFacu);
                var usuario = ctx.Usuarios.SingleOrDefault(c => c.Id == idUsuario);

                //estos dos controles no deberian ejecutarse xq deberia controlar que exista en el frontend
                if (facultad_update.Usuarios == null || !facultad_update.Usuarios.Any())
                {
                    facultad_update.Usuarios = new List<Usuario>();
                }
                if (usuario == null)
                {
                    usuario = new Usuario();
                }

                facultad_update.Usuarios.Add(usuario);
                ctx.SaveChanges();
            }
        }
    }
}