﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace udelaronline.Helpers
{
    public class MyException : Exception
    {
        public MyException(string Message) : base(Message)
        {

        }
    }
}
