﻿using dal;
using System.Collections.Generic;

namespace udelaronline.Models
{
    public class CursosFacultadViewModel
    {
        public virtual ICollection<Curso> Cursos { get; set; }
        public int IdFacultad { get; set; }
        public int IdCursoAIngresar { get; set; }
    }
}
