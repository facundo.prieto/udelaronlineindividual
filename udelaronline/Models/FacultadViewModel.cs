﻿using dal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace udelaronline.Models
{
    public class FacultadViewModel
    {
        public int Id { get; set; }

        //[Required(ErrorMessage = "Date is required")]
        [DisplayName("Fecha De Alta")]
        public DateTime FechaDeCreacion { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Abreviatura es requerido")]
        public string Abreviatura { get; set; }

        public virtual ICollection<Curso> Cursos { get; set; }

        //public virtual ICollection<UsuarioViewModel> Administradores { get; set; }

        public virtual ICollection<Usuario> EstudiantesInscriptos { get; set; }
    }
}
