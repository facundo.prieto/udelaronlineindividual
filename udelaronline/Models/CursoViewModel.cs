﻿using dal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace udelaronline.Models
{
    public class CursoViewModel
    {
        public int Id { get; set; }

        //[Required(ErrorMessage = "Date is required")]
        [DisplayName("Fecha De Alta")]
        public DateTime FechaDeCreacion { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Descripcion es requerida")]
        public string Descripcion { get; set; }

        public int FacultadId { get; set; }
        public string FacultadNombre { get; set; }
        public int ProfesorId { get; set; }
        public string ProfesorNombre { get; set; }

        public virtual ICollection<Usuario> Asistentes { get; set; }

        public virtual ICollection<Usuario> EstudiantesMatriculados { get; set; }
    }
}
