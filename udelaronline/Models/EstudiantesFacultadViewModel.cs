﻿using dal;
using System.Collections.Generic;

namespace udelaronline.Models
{
    public class EstudiantesFacultadViewModel
    {
        public virtual ICollection<Usuario> Estudiantes { get; set; }
        public int IdFacultad { get; set; }
        public string FacultadNombre { get; set; }
        public int IdUsuarioAIngresar { get; set; }
    }
}
