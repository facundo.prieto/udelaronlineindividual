﻿using dal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace udelaronline.Models
{
    public class UsuarioViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Ci es requerido")]
        public int Ci { get; set; }

        //[Required(ErrorMessage = "Date is required")]
        [DisplayName("Fecha De Alta")]
        public DateTime FechaDeAlta { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Apellido es requerido")]
        public string Apellido { get; set; }

        [Required(ErrorMessage = "Direccion de correo es requerida")]
        [EmailAddress(ErrorMessage = "Direccion de correo no valida")]
        public string Correo { get; set; }
        public int Telefono { get; set; }
        public int Rol { get; set; }     //1:SuperAdmin; 2:Admin; 3:Estudiante

        public int FacultadId { get; set; }
        public string FacultadNombre { get; set; }

        public virtual ICollection<Curso> CursosQueAsiste { get; set; }
        public virtual ICollection<Curso> CursosMatriculado { get; set; }
    }
}
