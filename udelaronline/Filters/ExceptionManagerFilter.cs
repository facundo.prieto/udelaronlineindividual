﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using udelaronline.Helpers;

namespace udelaronline.Filters
{
    public class ExceptionManagerFilter : IExceptionFilter
    {
        private readonly IWebHostEnvironment _hostingEnviroment;
        private readonly IModelMetadataProvider _modelMetadataProvider;

        public ExceptionManagerFilter(IWebHostEnvironment hostingEnviroment, IModelMetadataProvider modelMetadataProvider)
        {
            this._hostingEnviroment = hostingEnviroment;
            this._modelMetadataProvider = modelMetadataProvider;
        }
        public void OnException(ExceptionContext context)
        {
            if(context.Exception is MyException)
            {
                context.Result = new JsonResult("Fallo algo en la app " + _hostingEnviroment.ApplicationName + " la excepcion del tipo: " + context.Exception.GetType());
            }
        }
    }
}
