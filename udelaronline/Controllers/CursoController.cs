﻿using System;
using System.Collections.Generic;
using bl;
using dal;
using Microsoft.AspNetCore.Mvc;
using udelaronline.Models;

namespace udelaronline.Controllers
{
    public class CursoController : Controller
    {
        private CursoManager _cursoManager;
        public CursoController()
        {
            _cursoManager = new CursoManager();
        }
        public IActionResult Index()
        {
            var cursos = _cursoManager.lists();
            return View(cursos);
        }

        public IActionResult Add()
        {
            var curso = new CursoViewModel();
            return View(curso);    //si quiero que vaya a una vista que no se llame igual que la accion le pongo View("Add2",curso); 
        }

        [HttpPost]
        public IActionResult Add(CursoViewModel model)
        {
            if (ModelState.IsValid)
            {
                var curso = new Curso()
                {
                    Nombre = model.Nombre,
                    Descripcion = model.Descripcion,
                    FechaDeCreacion = DateTime.Now,
                    //Asistentes = new List<Asistente>(),
                    //EstudiantesMatriculados = new List<UsuarioCurso>()
                };
                _cursoManager.add(curso);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public IActionResult Delete(int id)
        {
            _cursoManager.delete(id);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var curso = _cursoManager.get(id);
            var model = new CursoViewModel()
            {
                Id = curso.Id,
                Nombre = curso.Nombre,
                Descripcion = curso.Descripcion
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(CursoViewModel model)
        {
            if (ModelState.IsValid)
            {
                var curso = new Curso()
                {
                    Id = model.Id,
                    Nombre = model.Nombre,
                    Descripcion = model.Descripcion
                    //Asistentes = model.Asistentes,
                    //EstudiantesMatriculados = model.EstudiantesMatriculados
                };
                _cursoManager.edit(curso);
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }
}
