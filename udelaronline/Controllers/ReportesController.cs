﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bl;
using dal;
using Microsoft.AspNetCore.Mvc;
using udelaronline.Models;

namespace udelaronline.Controllers
{
    public class ReportesController : Controller
    {
        private FacultadManager _facultadManager;
        private UsuarioManager _usuarioManager;

        public ReportesController()
        {
            _facultadManager = new FacultadManager();
            _usuarioManager = new UsuarioManager();
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ReporteDeUsuariosPorFacultad()
        {
            var model = new List<EstudiantesFacultadViewModel>();

            var facultades = _facultadManager.lists();
            foreach (var facu in facultades)
            {
                var usuarios = _usuarioManager.getUsuariosEnFacultad(facu.Id);
                var estEnFacu = new EstudiantesFacultadViewModel()
                {
                    IdFacultad = facu.Id,
                    FacultadNombre = facu.Nombre,
                    Estudiantes = usuarios
                };
                model.Add(estEnFacu);
            }

            return View(model);
        }

        public IActionResult NoImplementado()
        {
            return View();
        }
        /*

        public IActionResult UsuariosEnFac(int id)
        {
            var facu = _facultadManager.get(id);
            var model = new FacultadViewModel()
            {
                Id = facu.Id,
                Nombre = facu.Nombre,
                Abreviatura = facu.Abreviatura,
                EstudiantesInscriptos = _usuarioManager.getUsuariosEnFacultad(facu.Id)
            };
            return View(model);
        }

        public IActionResult SelecionUsuario(int id)
        {
            //Console.WriteLine("SelecionCurso idFacu  " + id);
            var usuarios = _usuarioManager.lists();
            var model = new EstudiantesFacultadViewModel()
            {
                IdFacultad = id,
                Estudiantes = usuarios
            };
            return View(model);
        }


        [HttpPost]
        public IActionResult SelecionUsuario(EstudiantesFacultadViewModel model)
        {
            bool existeUsuario = false;
            var totalUsuarios = _usuarioManager.lists();
            foreach (Usuario auxU in totalUsuarios)
            {
                if (auxU.Id == model.IdUsuarioAIngresar)
                {
                    existeUsuario = true;
                }
            }
            //Console.WriteLine(existeUsuario);
            if (ModelState.IsValid && existeUsuario)
            {
                _facultadManager.agregarEstudiante(model.IdFacultad, model.IdUsuarioAIngresar);
                return RedirectToAction("Index");
            }
            return View(model);
        }
        */
    }
}
