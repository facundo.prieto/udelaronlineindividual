﻿using System;
using bl;
using dal;
using Microsoft.AspNetCore.Mvc;
using udelaronline.Models;

namespace udelaronline.Controllers
{
    public class UsuarioController : Controller
    {
        private UsuarioManager _usuarioManager;
        public UsuarioController()
        {
            _usuarioManager = new UsuarioManager();
        }
        public IActionResult Index()
        {
            var usuarios = _usuarioManager.lists();
            return View(usuarios);
        }
        public IActionResult Add()
        {
            var usuario = new UsuarioViewModel();
            return View(usuario);    //si quiero que vaya a una vista que no se llame igual que la accion le pongo View("Add2",usuario); 
        }

        [HttpPost]
        public IActionResult Add(UsuarioViewModel model)
        {
            if (ModelState.IsValid)
            {
                var usuario = new Usuario()
                {
                    Nombre = model.Nombre,
                    Apellido = model.Apellido,
                    FechaDeAlta = DateTime.Now,
                    Correo = model.Correo,
                    Telefono = model.Telefono
                };
                _usuarioManager.add(usuario);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public IActionResult Delete(int id)
        {
            _usuarioManager.delete(id);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var usuario = _usuarioManager.get(id);
            var model = new UsuarioViewModel()
            {
                Id = usuario.Id,
                Nombre = usuario.Nombre,
                Apellido = usuario.Apellido,
                Correo = usuario.Correo,
                Telefono = usuario.Telefono
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(UsuarioViewModel model)
        {
            if (ModelState.IsValid)
            {
                var usuario = new Usuario()
                {
                    Id = model.Id,
                    Nombre = model.Nombre,
                    Apellido = model.Apellido,
                    Correo = model.Correo,
                    //Comments = new List<Comment>()
                };
                _usuarioManager.edit(usuario);
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }
}
