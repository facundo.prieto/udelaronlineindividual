﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bl;
using dal;
using Microsoft.AspNetCore.Mvc;
using udelaronline.Models;

namespace udelaronline.Controllers
{
    public class FacultadController : Controller
    {
        private FacultadManager _facultadManager;
        private CursoManager _cursoManager;
        private UsuarioManager _usuarioManager;

        public FacultadController()
        {
            _facultadManager = new FacultadManager();
            _cursoManager = new CursoManager();
            _usuarioManager = new UsuarioManager();
        }
        public IActionResult Index()
        {
            var facultades = _facultadManager.lists();
            return View(facultades);
        }
        public IActionResult Add()
        {
            var facu = new FacultadViewModel();
            return View(facu);    //si quiero que vaya a una vista que no se llame igual que la accion le pongo View("Add2",facu); 
        }

        [HttpPost]
        public IActionResult Add(FacultadViewModel model)
        {
            if (ModelState.IsValid)
            {
                var facu = new Facultad()
                {
                    Nombre = model.Nombre,
                    Abreviatura = model.Abreviatura,
                    FechaDeCreacion = DateTime.Now,
                    Cursos = new List<Curso>(),
                    //Administradores = new List<Usuario>(),
                    //EstudiantesInscriptos = new List<Usuario>()
                };
                _facultadManager.add(facu);
                return RedirectToAction("Index");   //return RedirectToAction("Index", "Home", new { mensaje = "Password incorrecta" });
            }
            return View(model);
        }

        public IActionResult Delete(int id)
        {
            _facultadManager.delete(id);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var facu = _facultadManager.get(id);
            var model = new FacultadViewModel()
            {
                Id = facu.Id,
                Nombre = facu.Nombre,
                Abreviatura = facu.Abreviatura
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(FacultadViewModel model)
        {
            if (ModelState.IsValid)
            {
                var facu = new Facultad()
                {
                    Id = model.Id,
                    Nombre = model.Nombre,
                    Abreviatura = model.Abreviatura
                };
                _facultadManager.edit(facu);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public IActionResult CursosEnFac(int id)
        {
            var facu = _facultadManager.get(id);
            var model = new FacultadViewModel()
            {
                Id = facu.Id,
                Nombre = facu.Nombre,
                Abreviatura = facu.Abreviatura,
                Cursos = _cursoManager.getCursosEnFacultad(facu.Id)
            };
            return View(model);
        }

        public IActionResult SelecionCurso(int id)
        {
            //Console.WriteLine("SelecionCurso idFacu  " + id);
            var cursos = _cursoManager.lists();
            var model = new CursosFacultadViewModel()
            {
                IdFacultad = id,
                Cursos = cursos
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult SelecionCurso(CursosFacultadViewModel model)
        {
            bool existeCurso = false;
            var totalCursos = _cursoManager.lists();
            foreach (Curso auxC in totalCursos)
            {
                if (auxC.Id == model.IdCursoAIngresar)
                {
                    existeCurso = true;
                }
            }
            Console.WriteLine(existeCurso);
            if (ModelState.IsValid && existeCurso)
            {
                _facultadManager.agregarCurso(model.IdFacultad, model.IdCursoAIngresar);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public IActionResult UsuariosEnFac(int id)
        {
            var facu = _facultadManager.get(id);
            var model = new FacultadViewModel()
            {
                Id = facu.Id,
                Nombre = facu.Nombre,
                Abreviatura = facu.Abreviatura,
                EstudiantesInscriptos = _usuarioManager.getUsuariosEnFacultad(facu.Id)
            };
            return View(model);
        }

        public IActionResult SelecionUsuario(int id)
        {
            //Console.WriteLine("SelecionCurso idFacu  " + id);
            var usuarios = _usuarioManager.lists();
            var model = new EstudiantesFacultadViewModel()
            {
                IdFacultad = id,
                Estudiantes = usuarios
            };
            return View(model);
        }

        
        [HttpPost]
        public IActionResult SelecionUsuario(EstudiantesFacultadViewModel model)
        {
            bool existeUsuario = false;
            var totalUsuarios = _usuarioManager.lists();
            foreach (Usuario auxU in totalUsuarios)
            {
                if (auxU.Id == model.IdUsuarioAIngresar)
                {
                    existeUsuario = true;
                }
            }
            //Console.WriteLine(existeUsuario);
            if (ModelState.IsValid && existeUsuario)
            {
                _facultadManager.agregarEstudiante(model.IdFacultad, model.IdUsuarioAIngresar);
                return RedirectToAction("Index");
            }
            return View(model);
        }

    }
}
