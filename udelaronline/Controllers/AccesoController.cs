﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using udelaronline.Models;

namespace udelaronline.Controllers
{
    public class AccesoController : Controller
    {
        //private UsuarioController _usuarioManager;

        // GET: Acceso
        public ActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Login(string User, string Pass)
        {
            try
            {
                //UsuarioViewModel user = null;
                /*
                using (Models.MiSistemaEntities db = new Models.MiSistemaEntities())
                {
                    var oUser = (from d in db.usuario
                                    where d.email == User.Trim() && d.password == Pass.Trim()
                                    select d).FirstOrDefault();
                if (user == null)
                {
                    ViewBag.Error = "Usuario o contraseña invalida";
                    return View();
                }
                Session["User"] = user;   //debería ser un UsuarioViewModel
                }*/

                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }
    }
}
