﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using udelaronline.Filters;
using udelaronline.Helpers;

namespace udelaronline.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [TypeFilter(typeof(ExceptionManagerFilter))]
    public class TestExceptionController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            throw new MyException("error prueba");
        }
    }
}
