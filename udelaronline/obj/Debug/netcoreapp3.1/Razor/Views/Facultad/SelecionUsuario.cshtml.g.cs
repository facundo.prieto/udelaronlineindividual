#pragma checksum "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d500f4eabf737d03e65f943532d8848368efc673"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Facultad_SelecionUsuario), @"mvc.1.0.view", @"/Views/Facultad/SelecionUsuario.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\TSI .NET\udelaronline\udelaronline\Views\_ViewImports.cshtml"
using udelaronline;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\TSI .NET\udelaronline\udelaronline\Views\_ViewImports.cshtml"
using udelaronline.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d500f4eabf737d03e65f943532d8848368efc673", @"/Views/Facultad/SelecionUsuario.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"55612948886a7bec94b88b8ddd422894bd23a173", @"/Views/_ViewImports.cshtml")]
    public class Views_Facultad_SelecionUsuario : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<EstudiantesFacultadViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"
<h2>Selecione un estudiante </h2>
<table class=""table"">
    <thead>
        <tr>
            <th>
                Id
            </th>
            <th>
                Nombre
            </th>
            <th>
                Apellido
            </th>
            <th>
                Correo
            </th>
            <th>

            </th>
        </tr>
    </thead>
    <tbody>
");
#nullable restore
#line 25 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
         if (Model.Estudiantes != null && Model.Estudiantes.Any())
        {
            

#line default
#line hidden
#nullable disable
#nullable restore
#line 27 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
             foreach (var curso in Model.Estudiantes)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <tr>\r\n                    <td>\r\n                        ");
#nullable restore
#line 31 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
                   Write(Html.DisplayFor(u => curso.Id));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 34 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
                   Write(Html.DisplayFor(u => curso.Nombre));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 37 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
                   Write(Html.DisplayFor(u => curso.Apellido));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 40 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
                   Write(Html.DisplayFor(u => curso.Correo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                </tr>\r\n");
#nullable restore
#line 43 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
            }

#line default
#line hidden
#nullable disable
#nullable restore
#line 43 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
             
        }
        else
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td>\r\n                    No hay usuarios en el sistema por el momento.\r\n                </td>\r\n            </tr>\r\n");
#nullable restore
#line 52 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </tbody>\r\n</table>\r\n\r\n");
#nullable restore
#line 56 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
 using (Html.BeginForm())
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <fieldset>\r\n        <legend>Ingrese el Id del usuario</legend>\r\n        <div class=\"editor-field\">\r\n            ");
#nullable restore
#line 61 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
       Write(Html.EditorFor(model => model.IdUsuarioAIngresar));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </div>\r\n        ");
#nullable restore
#line 63 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
   Write(Html.HiddenFor(model => model.IdFacultad));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 64 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
   Write(Html.HiddenFor(model => model.Estudiantes));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        <div class=\"buttons\">\r\n            <input type=\"submit\" value=\"Confirmar\" />\r\n        </div>\r\n    </fieldset>\r\n");
#nullable restore
#line 69 "D:\TSI .NET\udelaronline\udelaronline\Views\Facultad\SelecionUsuario.cshtml"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<EstudiantesFacultadViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
