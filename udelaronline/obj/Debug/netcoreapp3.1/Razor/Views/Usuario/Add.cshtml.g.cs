#pragma checksum "D:\TSI .NET\udelaronline\udelaronline\Views\Usuario\Add.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "819669d9e5eb85d146c53055d5283af174238482"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Usuario_Add), @"mvc.1.0.view", @"/Views/Usuario/Add.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\TSI .NET\udelaronline\udelaronline\Views\_ViewImports.cshtml"
using udelaronline;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\TSI .NET\udelaronline\udelaronline\Views\_ViewImports.cshtml"
using udelaronline.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"819669d9e5eb85d146c53055d5283af174238482", @"/Views/Usuario/Add.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"55612948886a7bec94b88b8ddd422894bd23a173", @"/Views/_ViewImports.cshtml")]
    public class Views_Usuario_Add : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<UsuarioViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "D:\TSI .NET\udelaronline\udelaronline\Views\Usuario\Add.cshtml"
 using (Html.BeginForm())
{

#line default
#line hidden
#nullable disable
            WriteLiteral("<fieldset>\r\n    <legend>Ingresar usuario</legend>\r\n    <div class=\"editor-label\">\r\n        ");
#nullable restore
#line 7 "D:\TSI .NET\udelaronline\udelaronline\Views\Usuario\Add.cshtml"
   Write(Html.LabelFor(model => model.Nombre));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"editor-field\">\r\n        ");
#nullable restore
#line 10 "D:\TSI .NET\udelaronline\udelaronline\Views\Usuario\Add.cshtml"
   Write(Html.TextBoxFor(model => model.Nombre));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 11 "D:\TSI .NET\udelaronline\udelaronline\Views\Usuario\Add.cshtml"
   Write(Html.ValidationMessageFor(model => model.Nombre, "", new { @class = "custom_error" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"editor-label\">\r\n        ");
#nullable restore
#line 14 "D:\TSI .NET\udelaronline\udelaronline\Views\Usuario\Add.cshtml"
   Write(Html.LabelFor(model => model.Apellido));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"editor-field\">\r\n        ");
#nullable restore
#line 17 "D:\TSI .NET\udelaronline\udelaronline\Views\Usuario\Add.cshtml"
   Write(Html.TextBoxFor(model => model.Apellido));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 18 "D:\TSI .NET\udelaronline\udelaronline\Views\Usuario\Add.cshtml"
   Write(Html.ValidationMessageFor(model => model.Apellido, "", new { @class = "custom_error" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"editor-label\">\r\n        ");
#nullable restore
#line 21 "D:\TSI .NET\udelaronline\udelaronline\Views\Usuario\Add.cshtml"
   Write(Html.LabelFor(model => model.Correo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"editor-field\">\r\n        ");
#nullable restore
#line 24 "D:\TSI .NET\udelaronline\udelaronline\Views\Usuario\Add.cshtml"
   Write(Html.TextBoxFor(model => model.Correo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 25 "D:\TSI .NET\udelaronline\udelaronline\Views\Usuario\Add.cshtml"
   Write(Html.ValidationMessageFor(model => model.Correo, "", new { @class = "custom_error" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"buttons\">\r\n        <input type=\"submit\" value=\"Add\" />\r\n    </div>\r\n</fieldset>\r\n");
#nullable restore
#line 31 "D:\TSI .NET\udelaronline\udelaronline\Views\Usuario\Add.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<UsuarioViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
