﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace dal.Migrations
{
    public partial class migration2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cursos_Usuarios_ProfesorId",
                table: "Cursos");

            migrationBuilder.DropForeignKey(
                name: "FK_Usuarios_Facultades_FacultadId",
                table: "Usuarios");

            migrationBuilder.AlterColumn<int>(
                name: "FacultadId",
                table: "Usuarios",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ProfesorId",
                table: "Cursos",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "FacultadId",
                table: "Cursos",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Cursos_Usuarios_ProfesorId",
                table: "Cursos",
                column: "ProfesorId",
                principalTable: "Usuarios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Usuarios_Facultades_FacultadId",
                table: "Usuarios",
                column: "FacultadId",
                principalTable: "Facultades",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cursos_Usuarios_ProfesorId",
                table: "Cursos");

            migrationBuilder.DropForeignKey(
                name: "FK_Usuarios_Facultades_FacultadId",
                table: "Usuarios");

            migrationBuilder.AlterColumn<int>(
                name: "FacultadId",
                table: "Usuarios",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ProfesorId",
                table: "Cursos",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FacultadId",
                table: "Cursos",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Cursos_Usuarios_ProfesorId",
                table: "Cursos",
                column: "ProfesorId",
                principalTable: "Usuarios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usuarios_Facultades_FacultadId",
                table: "Usuarios",
                column: "FacultadId",
                principalTable: "Facultades",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
