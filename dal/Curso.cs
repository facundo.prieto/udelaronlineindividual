﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dal
{
    public class Curso
    {
        [Key]
        public int Id { get; set; }
        /*[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]*/
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaDeCreacion { get; set; }
        public int? FacultadId { get; set; }        //el ? hace que permina nulos
        public Facultad Facultad { get; set; }
        public int? ProfesorId { get; set; }        //el ? hace que permina nulos
        public Usuario Profesor { get; set; }
        public virtual ICollection<Asistente> Asistentes { get; set; }
        public virtual ICollection<UsuarioCurso> EstudiantesMatriculados { get; set; }
    }
}