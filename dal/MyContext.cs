﻿using Microsoft.EntityFrameworkCore;

/*
 * d:
 * cd "TSI .NET"\udelaronline\dal
 * dotnet ef migrations add migration1
 * dotnet ef database update
 * 
 * reestablecer ids autogenerados:
 * DBCC CHECKIDENT (mytable, RESEED, 0)
 */

namespace dal
{
    public class MyContext : DbContext
    {

        public MyContext() : base()
        {
        }

        public DbSet<Facultad> Facultades { get; set; }
        public DbSet<Curso> Cursos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        //public DbSet<UsuarioCurso> UsuarioCursos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Facultad>().HasMany(c => c.Cursos).WithOne(c => c.Facultad).HasForeignKey(c => c.FacultadId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<UsuarioCurso>().HasKey(a => new { a.CursoId, a.UsuarioId });
            modelBuilder.Entity<UsuarioCurso>().HasOne<Usuario>(sc => sc.Usuario).WithMany(s => s.CursosMatriculado).HasForeignKey(sc => sc.UsuarioId).OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<UsuarioCurso>().HasOne<Curso>(sc => sc.Curso).WithMany(s => s.EstudiantesMatriculados).HasForeignKey(sc => sc.CursoId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Asistente>().HasKey(a => new { a.CursoId, a.UsuarioId });
            modelBuilder.Entity<Asistente>().HasOne<Usuario>(sc => sc.Usuario).WithMany(s => s.CursosQueAsiste).HasForeignKey(sc => sc.UsuarioId).OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Asistente>().HasOne<Curso>(sc => sc.Curso).WithMany(s => s.Asistentes).HasForeignKey(sc => sc.CursoId).OnDelete(DeleteBehavior.NoAction);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Server=DESKTOP-SFQ32L5\SQLEXPRESS;Database=udelaronline1;Integrated Security=True;Trusted_Connection=True;MultipleActiveResultSets=true");
        }
    }
}
