﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dal
{
    public class Facultad
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Abreviatura { get; set; }
        public DateTime FechaDeCreacion { get; set; }
        public virtual ICollection<Curso> Cursos { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
        //public virtual ICollection<Usuario> Administradores { get; set; }
        //public virtual ICollection<Usuario> EstudiantesInscriptos { get; set; }
    }
}
