﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dal
{
    public class Asistente
    {
        //[Key]
        public int CursoId { get; set; }
        //[ForeignKey("CursoId")]
        public Curso Curso { get; set; }

        //[Key]
        public int UsuarioId { get; set; }
        //[ForeignKey("UsuarioId")]
        public Usuario Usuario { get; set; }
    }
}
