﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dal
{
    public class UsuarioCurso
    {
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

        public int CursoId { get; set; }
        public Curso Curso { get; set; }
    }
}
