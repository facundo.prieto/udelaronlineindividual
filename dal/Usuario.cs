﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dal
{
    public class Usuario
    {
        [Key]
        public int Id { get; set; }
        public int Ci { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public DateTime FechaDeAlta { get; set; }
        public string Correo { get; set; }
        public int Telefono { get; set; }

        [EnumDataType(typeof(Rol))]
        public Rol Rol { get; set; }     //0:SuperAdmin; 1:Admin; 2:Profesor; 3:Estudiante
        public int? FacultadId { get; set; }        //el ? hace que permina nulos
        public Facultad Facultad { get; set; }

        //public Image FotoDePerfil { get; set; }

        public virtual ICollection<Asistente> CursosQueAsiste { get; set; }
        public virtual ICollection<UsuarioCurso> CursosMatriculado { get; set; }
    }
    public enum Rol
    {
        SuperAdmin = 0,
        Admin = 1,
        Profesor = 2,
        Estudiante = 3
    }
}